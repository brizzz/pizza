//Коллекция, хранящая ассортимент продукции
var Catalog = Backbone.Collection.extend({
        getPizza: function(id) {
            return this.get(id);
        },
        getAdditions: function(id) {
            return this.get(id).get('additions');
        },
        getAddition: function(pizzaId, id) {
            return _.find(this.getAdditions(pizzaId), function(addition) {
                return addition.id == id;
            });
        }
    }
);

var catalog = new Catalog([
    {id: 1, name: "Пицца1", price: "100", additions: [
        {id:1, name: "Добавка 1 к пицце 1", price: 20},
        {id:2, name: "Добавка 2 к пицце 1", price: 30},
        {id:3, name: "Добавка 3 к пицце 1", price: 40}
    ]},

    {id: 2, name: "Пицца2", price: "200", additions: [
        {id:1, name: "Добавка 1 к пицце 2", price: 40}
    ]},
    {id: 3, name: "Пицца без добавок", price: "250"}
]);

//Элемент списка заказов
var Item = Backbone.Model.extend({
    defaults: {
        id: undefined,
        quantity: 1,
        additions: new Backbone.Collection(),
        price: 0
    },
    summ: function() {
        if (!this.id) {
            return 0;
        }

        var summ = parseInt(this.get('price'));
        this.get('additions').each(function(item){
            summ += item.get('price');
        });

        return summ*this.get('quantity');
    }
});

//Список заказов
var Items = Backbone.Collection.extend({
    model: Item,
    total: function(){
        var total = 0;
        this.each(function(pizza){
            total += pizza.summ();
        });

        return total;
    }
});

//View для элемента зказа. Отвечает за отображение и обработку действий пользователя над элементом заказа
var ItemView = Backbone.View.extend({
    tagName: 'tr',
    className: 'item',
    template: _.template($('#item-template').html()),
    events: {
        "click .remove": "clear",
        "change select.additions": "setAdditions",
        "change select.pizza": "setPizza",
        "change .quantity": "setQuantity"
    },
    setQuantity: function() {
        var $quantity = this.$('.quantity');

        //обрабатываем не верный ввод
        if (!$quantity.val() || parseInt($quantity.val()) <= 0) {
            $quantity.val(1);
        }

        //синхронизируем значение в поле с моделью
        this.model.set('quantity', $quantity.val());
    },
    //выбираем пиццу
    setPizza: function() {
        var id = this.$('.pizza option:selected').attr('value');
        var pizza = catalog.getPizza(id);
        this.model.set({
            id: pizza.id,
            name: pizza.get('name'),
            quantity: 1,
            price: pizza.get('price'),
            additions: new Backbone.Collection()
        });

        appView.render();
    },
    //выбираем дополнительный ингридиент
    setAdditions: function() {
        var additions = this.model.get('additions');
        var limit = 0;

        //сначала обнуляем список доп ингридиентов
        additions.reset();

        //а теперь добавляем каждый выбранный ингридиент в модель
        _.each(this.$('.additions option:selected'), function(option){
            var additionId = $(option).attr('value');

            if (additionId) {
                if (limit >=2) {
                    alert('В пиццу можно добавить только 2 дополнительных ингридиента!');
                } else {
                    additions.add(catalog.getAddition(this.model.id, additionId));
                    limit++;
                }
            }
        }, this);

        appView.render();
    },
    //удаляем пиццу
    clear: function() {
        if (confirm('Вы уверены?')) {
            appView.collection.remove(this.model);
        }
    },
    render: function() {
        this.$el.html(this.template(_.extend(
            this.model.toJSON(),
            {
                model: this.model,
                catalog: catalog
            }
        )));
        return this;
    }
});

//View приложения. Отвечает за отображение и обработку приложения в целом
var AppView = Backbone.View.extend({
    el: ".pizza-app",
    initialize: function() {
        this.listenTo(this.collection, 'all', this.render);
        this.render();
    },
    events: {
        "click .add": "addItem",
        "click .submit": "submitOrder"
    },
    //обрабатываем заказ
    submitOrder: function() {
        //выводим результат
        $('#order .modal-body .list').html(_.template($('#order-template').html())({order: this.collection}));
        $('#order').modal();

        //отправляем результат на сервер для сохранения в файл
        $.ajax({
            url: '/save.php',
            method: "POST",
            data: JSON.stringify(this.collection),
            dataType: "json"
        }).success(function(data){
            $('#order .modal-body .file').html('Заказ сохранен в файле ' +'<a target="_blank" href="/'+data.path+'">/'+data.path+'</a>');
        });
    },
    //добавляем элемент заказа
    addItem: function() {
        this.collection.add(new Item());
    },
    render: function(){
        //рендерим список элементов заказ
        this.$('.items').html('');
        this.collection.each(function(item){
            var itemView = new ItemView({model: item});
            this.$('.items').append(itemView.render().el);
        });

        //отображение ИТОГО и кнопки Заказать
        if (this.collection.total() > 0) {
            this.$('.submit').removeAttr('disabled');
            this.$('.total').text(this.collection.total());
        } else {
            this.$('.submit').attr('disabled', 'disabled');
            this.$('.total').text('-');
        }

        return this;
    }
});

//инициализируем приложение
var appView = new AppView({collection: new Items()});


//debug only
//var additions = new Backbone.Collection([
//    {id:1, name: "Добавка 1 к пицце 1", price: 20}
//]);
//var data = [
//    {id: 1, name: "Пицца1", price: "100", additions: additions},
//    {id: 2, name: "Пицца2", price: "200"}
//];
//var pizzas = new Items(data);
//var appView = new AppView({collection: pizzas});
