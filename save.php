<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST'){

    //читаем данные из запроса
    $json = file_get_contents('php://input');

    $ordersDir = 'orders';

    //создаем папку если не создана
    if (!is_dir($ordersDir)) {
        $res = mkdir($ordersDir);
    }

    //сохраняем заказа в формате JSON в файле
    $fileName = $ordersDir.DIRECTORY_SEPARATOR.date('Y-m-d_H:i:s').'.json';
    $fp = fopen($fileName,"wb");
    fwrite($fp,$json);
    fclose($fp);

    //возвращаем путь к файлу
    echo json_encode(array('path' => $fileName));
}